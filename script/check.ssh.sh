#!/bin/bash

ssh=$(pidof sshd)

if [[ $ssh = "" ]]; then
	stssh=$(dpkg -l	| grep ssh)
		if [[ $stssh = "" ]]; then
			echo [/!\] ssh : pas installé [/!\]
                	apt-get install ssh --yes
		else
         	        echo [...] ssh : installé [...]
			echo [/!\] ssh : le service n est pas lancé [/!\]
			echo [...] ssh : lancement du service [...]
			/etc/init.d/ssh start
 	        fi
else 
	echo [...] ssh : fonctionne [...]
fi

