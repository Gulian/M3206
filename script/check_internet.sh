#!/bin/bash 

echo [...] Checking internet connection [...]

nc -z 8.8.8.8 53  >/dev/null 2>&1
test=$?
if [ $test -eq 0 ]; then
    echo [...] Internet access OK [..]
else
    echo [/!\] Not connected to Internet    [/!\] 
    echo [/!\] Please check configuration   [/!\]
fi
