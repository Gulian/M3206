#!/bin/bash

for variable in 'git' 'tmux' 'htop' 'vim'
do 
	test=$(dpkg -l | grep $variable)
	if [[ $test = "" ]]; then
		echo [/!\] $variable: pas installé [/!\]
		apt-get install $variable --yes
	else
		echo [...]$variable : installé [...]
	fi
done
