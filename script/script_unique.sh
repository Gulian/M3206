
#!/bin/bash 

echo [...] Checking internet connection [...]

nc -z 8.8.8.8 53  >/dev/null 2>&1
test=$?
if [ $test -eq 0 ]; then
    echo [...] Internet access OK [..]
else
    echo [/!\] Not connected to Internet    [/!\] 
    echo [/!\] Please check configuration   [/!\]
fi
#!/bin/bash

for variable in 'git' 'tmux' 'htop' 'vim'
do 
	test=$(dpkg -l | grep $variable)
	if [[ $test = "" ]]; then
		echo [/!\] $variable: pas installé [/!\]
		apt-get install $variable --yes
	else
		echo [...]$variable : installé [...]
	fi
done
#!/bin/bash

iam=`whoami`

if [ $iam != root ]; then
        echo [/!\] Vous devez être super-utilisateur [/!\] 
fi


echo [...] update database [...]
apt-get update

echo [...] upgrade system [...]
apt-get dist-upgrade --yes 
#!/bin/bash

ssh=$(pidof sshd)

if [[ $ssh = "" ]]; then
	stssh=$(dpkg -l	| grep ssh)
		if [[ $stssh = "" ]]; then
			echo [/!\] ssh : pas installé [/!\]
                	apt-get install ssh --yes
		else
         	        echo [...] ssh : installé [...]
			echo [/!\] ssh : le service n est pas lancé [/!\]
			echo [...] ssh : lancement du service [...]
			/etc/init.d/ssh start
 	        fi
else 
	echo [...] ssh : fonctionne [...]
fi

