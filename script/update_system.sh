#!/bin/bash

iam=`whoami`

if [ $iam != root ]; then
        echo [/!\] Vous devez être super-utilisateur [/!\] 
fi


echo [...] update database [...]
apt-get update

echo [...] upgrade system [...]
apt-get dist-upgrade --yes 
